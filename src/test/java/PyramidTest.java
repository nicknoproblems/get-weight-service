import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import weight.domain.IPyramid;
import weight.domain.Pyramid;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by NKartashov on 29.08.2014.
 * Это не юнит-тест , т.к. эталонный реализации алгоритма у меня нет , мне не с чем сравнивать.
 * Поэтому эти тесты носят больше демонстрационный характер.
 */
@RunWith(Parameterized.class)
public class PyramidTest {

    IPyramid pyramid = new Pyramid(50, 5);
    long level;
    long index;

    public PyramidTest(long level, long index) {
        this.level = level;
        this.index = index;
    }


    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{{0, 0},
                {1, 0}, {1, 1},
                {2, 0}, {2, 1}, {2, 2},
                {3, 0}, {3, 1}, {3, 2}, {3, 3}};
        return Arrays.asList(data);
    }

    @Test
    public void testAll() {
        System.out.println("Level:" + level + " Index:" + index + " TopMass:" + pyramid.getHumanEdgeWeight(level, index));
    }

}
