package weight.domain;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nickolas on 28.08.2014.
 */
@Component
public class Pyramid implements IPyramid {

    private static Map<Long, Double> cache = new HashMap<>();
    private static long blockWeight = 50;
    private static int levels = 4;

    public Pyramid(long blockWeight, int levels) {
        this.blockWeight = blockWeight;
        this.levels = levels;
    }

    //for spring
    @Deprecated
    public Pyramid() {
    }

    private static double massOfTopBlocks(long n) {
        double res = 0;
        if (n == 0) return blockWeight;
        Double cacheVal = cache.get(n);
        if (null != cacheVal) return cacheVal;
        for (int i = 1; i <= n; ++i) res = res + i;
        res = 50 * res;
        cache.put(n, res);
        return res;
    }

    public static int getLevels() {
        return levels;
    }

    public double getHumanEdgeWeight(long level, long index) {
        if (level >= levels || index > level) throw new RuntimeException("bigger level index then pyramide have");
        if (level == 0) return 0;
        double topMass = massOfTopBlocks(level);
        double massForCentricBlock = topMass / level;
        double massForOutsiderBlock;
        if (index == 0 || index == level) {
            massForOutsiderBlock = massForCentricBlock / 2;
            return massForOutsiderBlock;
        }
        return massForCentricBlock;
    }

    public long getBlockWeight() {
        return blockWeight;
    }
}
