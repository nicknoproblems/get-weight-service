package weight.domain;

/**
 * Created by NKartashov on 28.08.2014.
 */
public interface IPyramid {

    double getHumanEdgeWeight(long level, long index);
}
