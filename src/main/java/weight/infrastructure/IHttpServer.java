package weight.infrastructure;

/**
 * Created by NKartashov on 28.08.2014.
 */
public interface IHttpServer {
    void run() throws InterruptedException;
}
