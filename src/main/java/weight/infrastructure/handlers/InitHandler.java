package weight.infrastructure.handlers;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.http.HttpServerCodec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by NKartashov on 28.08.2014.
 */
@Service
public class InitHandler extends ChannelInitializer {

    private GetWeightHandler buisnessLogicHandler;

    @Autowired
    public InitHandler(GetWeightHandler buisnessLogicHandler) {
        this.buisnessLogicHandler = buisnessLogicHandler;
    }

    @Override
    protected void initChannel(Channel ch) throws Exception {
        ChannelPipeline p = ch.pipeline();
        p.addLast(new HttpServerCodec(4096, 8192, 8192, false));
        p.addLast(buisnessLogicHandler);
    }
}
