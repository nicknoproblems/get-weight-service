package weight.infrastructure.handlers;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import weight.service.IPyramidService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static io.netty.handler.codec.http.HttpHeaders.Names.CONTENT_LENGTH;
import static io.netty.handler.codec.http.HttpHeaders.Names.CONTENT_TYPE;
import static io.netty.handler.codec.http.HttpResponseStatus.*;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;

/**
 * Created by NKartashov on 28.08.2014.
 */
@Service
@ChannelHandler.Sharable
public class GetWeightHandler extends SimpleChannelInboundHandler<HttpRequest> {

    public static final Pattern p = Pattern.compile("^/weight(/(\\d{1})/(\\d{1})$|\\?level=(\\d{1})\\&index=(\\d{1}))$");
    public static final DefaultFullHttpResponse Resp404 = createResponse(null, NOT_FOUND);
    public static final DefaultFullHttpResponse Resp400 = createResponse(null, BAD_REQUEST);
    private IPyramidService pyramid;

    private static DefaultFullHttpResponse createResponse(byte[] body, HttpResponseStatus status) {
        DefaultFullHttpResponse res;
        if (null != body) res = new DefaultFullHttpResponse(HTTP_1_1, status, Unpooled.wrappedBuffer(body), false);
        else res = new DefaultFullHttpResponse(HTTP_1_1, status);
        res.headers().set(CONTENT_TYPE, "text/plain");
        res.headers().set(CONTENT_LENGTH, res.content().readableBytes());
        return res;
    }

    private static Pair<String, String> ifMatchReturnParams(HttpRequest req) {
        String uri = req.getUri();
        Matcher matcher = p.matcher(uri);
        if (matcher.find()) {
            if (null != matcher.group(2)) return Pair.with(matcher.group(2), matcher.group(3));
            else return Pair.with(matcher.group(4), matcher.group(5));
        } else return Pair.with(null, null);
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        ctx.writeAndFlush(Resp400, ctx.voidPromise());
        ctx.close();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, HttpRequest req) throws Exception {
        Pair<String, String> routeMatch = ifMatchReturnParams(req);
        if (routeMatch.getValue0() == null) ctx.write(Resp404).addListener(ChannelFutureListener.CLOSE);
        else {
            long level = Long.parseLong(routeMatch.getValue0());
            long index = Long.parseLong(routeMatch.getValue1());
            String resp = String.valueOf(pyramid.getHumanEdgeWeight(level, index));
            FullHttpResponse response = createResponse(resp.getBytes(), OK);
            ctx.write(response).addListener(ChannelFutureListener.CLOSE);
        }
    }


    public IPyramidService getPyramid() {
        return pyramid;
    }

    @Autowired
    public void setPyramid(IPyramidService pyramid) {
        this.pyramid = pyramid;
    }
}
