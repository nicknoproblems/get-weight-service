package weight;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import weight.infrastructure.IHttpServer;

/**
 * Created by NKartashov on 28.08.2014.
 */
@Configuration
@ComponentScan(basePackages = "weight")
public class Application {

    public static void main(String[] args) throws InterruptedException {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(Application.class);
        //Arrays.asList(ctx.getBeanDefinitionNames()).forEach(System.out::println);
        IHttpServer server = ctx.getBean(IHttpServer.class);
        server.run();
    }
}
