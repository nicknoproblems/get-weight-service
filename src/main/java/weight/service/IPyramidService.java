package weight.service;

/**
 * Created by NKartashov on 28.08.2014.
 */
public interface IPyramidService {

    double getHumanEdgeWeight(long level, long index);

}
