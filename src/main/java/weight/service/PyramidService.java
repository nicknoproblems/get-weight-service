package weight.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import weight.domain.IPyramid;

/**
 * Created by NKartashov on 28.08.2014.
 */
@Service
public class PyramidService implements IPyramidService {
    private IPyramid pyramidImpl;

    @Autowired
    public PyramidService(IPyramid pyramidImpl) {
        this.pyramidImpl = pyramidImpl;
    }

    @Deprecated
    //for spring
    public PyramidService() {
    }

    @Override
    public double getHumanEdgeWeight(long level, long index) {
        return pyramidImpl.getHumanEdgeWeight(level, index);
    }

    public IPyramid getPyramidImpl() {
        return pyramidImpl;
    }

}
